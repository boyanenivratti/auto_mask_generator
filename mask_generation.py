import functools
import io

import numpy as np
from PIL import Image
try:
    from u2net import detect
except Exception as e:
    from auto_mask_generator.u2net import detect


def get_model(model_name):
    if model_name == "u2netp":
        return detect.load_model(model_name="u2netp")
    if model_name == "u2net_human_seg":
        return detect.load_model(model_name="u2net_human_seg")
    else:
        return detect.load_model(model_name="u2net")

# global model
model = get_model(model_name="u2net")


def generate_mask(
    image
):
    if isinstance(image, str):
        img = Image.open(image).convert("RGB")
    else:
        img = image

    mask = detect.predict(model, np.array(img)).convert("L")

    resized_mask = mask.resize(img.size, Image.LANCZOS)
    return resized_mask


if __name__ == "__main__":
    mask = generate_mask("sample_image.png")
    mask.save("output_mask.png")